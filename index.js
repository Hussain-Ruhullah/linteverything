module.exports = {
    "extends": ["standard-with-typescript", "plugin:react/recommended", 
    "plugin:react-hooks/recommended", "plugin:@typescript-eslint/recommended", 
    "eslint:recommended", "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:yaml/recommended","plugin:eslint-plugin-json-es/recommended"],
    "parserOptions": {
        "project": ["./tsconfig.eslint.json","@typescript-eslint/parser"],
    },
    "parser": "eslint-plugin-json-es",
    "plugins": ["@typescript-eslint","yaml"],
    "files": ["*.json"],
    "rules": {
        "linebreak-style": ["error", process.env.NODE_ENV === 'prod' ? "unix" : "windows"],
        "@typescript-eslint/linebreak-style": ["error", process.env.NODE_ENV === 'prod' ? "unix" : "windows"],
        "no-extra-semi": "off",
        "@typescript-eslint/no-extra-semi": ["off"],
        "quotes": ["error", "double"],
        "@typescript-eslint/quotes": ["error", "double"],
        "semi": ["error", "always"],
        "@typescript-eslint/semi": ["error", "always"],
        "space-before-function-paren": ["error", {
            "anonymous": "never",
            "named": "never",
            "asyncArrow": "never",
        }],
        "@typescript-eslint/space-before-function-paren": ["error", {
            "anonymous": "never",
            "named": "never",
            "asyncArrow": "never",
        }],
        "strict-boolean-expressions": ["off"],
        "@typescript-eslint/strict-boolean-expressions": ["off"],
        "indent": ["error", 4, {
            "SwitchCase": 4
        }],
        "@typescript-eslint/indent": ["error", 4, {
            "SwitchCase": 4
        }],
        "comma-dangle": ["error", "only-multiline"],
        "comma-dangle": ["error", "only-multiline"],
        "@typescript-eslint/member-delimiter-style": ["error", {
            "multiline": {
                "delimiter": "semi",
                "requireLast": true
            },
            "singleline": {
                "delimiter": "comma",
                "requireLast": false
            }
        }],
        "@typescript-eslint/member-delimiter-style": ["error", {
            "multiline": {
                "delimiter": "semi",
                "requireLast": true
            },
            "singleline": {
                "delimiter": "comma",
                "requireLast": false
            }
        }],
        // disable the rule for all files
        "@typescript-eslint/explicit-function-return-type": "off"

    },
    "overrides": [{
        "files": ["*.ts", "*.tsx", "**/*.test.js", "**/*.test.ts", "**/*.test.tsx","*.yaml", "*.yml",".json"],
        "rules": {
            "@typescript-eslint/explicit-function-return-type": ["error"]
        }
    }],
    
};
