# ecspand™ Eslint Setup for React

## set.&.forget

* Lints Typescript/react based on the latest standards
* Fixes issues and formatting errors with eslint only
* Lints + Fixes React-Typescript

## Setup vs code

Instructions for VS Code:

1. Uninstall Prettier and Tslint plugins if you have them installed.

2. Delete tslint.json and .prettierrc.json also remove tslint and prettier from your .vscode/extensions.json file

3. Now we need to setup some VS Code settings via `Code/File` → `Preferences` → `Settings`. It's easier to enter these settings while editing the `settings.json` file, so click the Open (Open Settings) icon in the top right corner:

  ```json
    {
      "editor.tabSize": 4,
      "editor.formatOnSave": true,
      "editor.codeActionsOnSave": {
          "source.fixAll": true
      },
      "files.exclude": {
          "**/.git": true,
          "**/.svn": true,
          "**/.hg": true,
          "**/CVS": true,
          "**/.DS_Store": true,
          "**/node_modules": true,
          "**/coverage": true
      },
      "eslint.validate": [
          "typescript",
          "javascript",
          "json",
          "jsonc"
      ],
      "[typescript]": {
          "editor.defaultFormatter": "dbaeumer.vscode-eslint"
      },
      "[javascript]": {
          "editor.defaultFormatter": "dbaeumer.vscode-eslint"
      },
      "eslint.format.enable": true,
      "eslint.alwaysShowStatus": true
    }
  ```

## Installing

1. If you don't already have a `package.json` file, create one with `npm init`.

2. Install the [ESLint package](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) and everything needed by the config:

```
npm install -D eslint typescript @ecspand/eslint-config-react@0.0.0-beta.0 eslint-plugin-react eslint-plugin-node eslint-plugin-import  @typescript-eslint/eslint-plugin eslint-plugin-promise standard eslint-config-standard-with-typescript  @typescript-eslint/eslint-plugin eslint-plugin-react-hooks eslint-plugin-json-es eslint-plugin-yaml
```

3. Create a `.eslintrc` file in the root of your project's directory (it should live where package.json does). Your `.eslintrc` file should look like this:

```json
{
  "extends": [
    "@ecspand/eslint-config-react"
  ]
}
```

Tip: You can alternatively put this object in your `package.json` under the property `"eslintConfig":`. This makes one less file in your project.

> Note: **In order to lint hidden files** (e.g. `.eslintrc.json`), you'll need to modify/create a `.eslintignore` in your project root with these contents:
`.eslintignore`:

```gitignore
# eslint ignores hidden files by default
!.*.json
**/node_modules
```

4. create a file called: `tsconfig.eslint.json` and past the code bellow

```json
{
  "compilerOptions": {
    "noImplicitAny": true,
    "esModuleInterop": true,
    "moduleResolution": "node",
    "module": "commonjs",
    "target": "ES2019",
    "sourceMap": true,
    "strict": true,
    "jsx": "react",
    "skipLibCheck": true,
    "resolveJsonModule": true,
    "outDir": "dist",
    "declaration": true,
    "strictNullChecks": false
  },
  "extends": "./tsconfig.json",
  "include": ["src/**/*.ts",".json"],
  "exclude": ["node_modules", "test", "dist", "scripts","**/*.test.ts"]
}
```

5. Edit package.json and add the following scripts

```json
{
      "scripts": {
        "lint": "eslint ./src ",
        "lint:fix": "eslint ./src --fix",
      }
}
```

6. Now you can manually lint your code by running `npm run lint` and fix all fixable issues with `npm run lint:fix`. You probably want your editor to do this though.

7. After attempting to lint your file for the first time, you may need to click on 'ESLint' in the bottom right and select 'Allow Everywhere' in the alert window.

8. restart VS code.

ps. Make sure you have a valid tsconfig.json file
